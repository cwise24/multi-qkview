#!/usr/bin/env python3

"""Import libraries"""

import os, shutil, json, requests, time
from user_info import bauth

url = "https://identity.account.f5.com/oauth2/ausp95ykc80HOU7SQ357/v1/token"
headers = {"accept": "application/json", "authorization": "Basic "+ bauth}
payload = {"grant_type": "client_credentials", "scope": "ihealth" }
session = requests.session()
r_token = session.post(url, headers=headers, data=payload)
r = r_token.content
r_json = json.loads(r)
rtoken = r_json.get("access_token")


print("Auth Token ", r_token.status_code)

for f in os.listdir('.'):
    if f.endswith('.qkview'):
        url1 = "https://ihealth2-api.f5.com/qkview-analyzer/api/qkviews"
        headers1 = {"Accept": "application/vnd.f5.ihealth.api", "user-agent": "FSE_QKapi", "Authorization": "Bearer "+rtoken }
        payload1 = {"visible_in_gui": "True"}
        fvar = {'qkview': open(f, 'rb')}
        r_Up = session.post(url1, headers=headers1, files=fvar, data=payload1)
        print("Upload Status ", r_Up.status_code)
