# multi-qkview

- This repo allows a F5 admin to generate multiple *QKVIEWS* and names them by inventory name (Ansible)
- Python program runs to upload qkviews to F5 iHealth

## Prior to Run
Edit file *user_info.py*, add Client ID and Client Secret obtained from iHealth->Settings

![Generate Token](img/iHealthToken.png)

[See Instructions](https://clouddocs.f5.com/api/ihealth/Authentication.html)

```
cID='<enter client id here>'
cSec='<enter client secret here>'
```
